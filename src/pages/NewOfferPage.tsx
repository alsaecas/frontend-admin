import React, { useEffect } from "react";
import { useNavigate } from 'react-router-dom';
import { useSessionStorage } from "../hooks/useSessionStorage";

export const NewOfferPage = () => {
    const loggedIn = useSessionStorage('sessionJWTToken');
    let navigate = useNavigate();
    useEffect(() => {
        if (!loggedIn) {
            return navigate('/login')
        }
    }, [loggedIn])

    return (
        <h1>WELCOME TO NEW OFFER PAGE!</h1>
    )
}
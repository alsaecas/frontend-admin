import React, { useEffect } from "react";
import { useNavigate } from 'react-router-dom';
import { useSessionStorage } from "../hooks/useSessionStorage";

export const HomePage = () => {
    const loggedIn = useSessionStorage('sessionJWTToken');
    let navigate = useNavigate();
    useEffect(() => {
        if (!loggedIn) {
            return navigate('/login')
        }
    }, [loggedIn])

    return (
        <h1>WELCOME TO DASHBOARD!</h1>
    )
}
import React, { useEffect } from "react";
import { useNavigate } from 'react-router-dom';
import { useSessionStorage } from "../hooks/useSessionStorage";

export const OfferPage = () => {
    const loggedIn = useSessionStorage('sessionJWTToken');
    let navigate = useNavigate();
    useEffect(() => {
        if (!loggedIn) {
            return navigate('/login')
        }
    }, [loggedIn])

    return (
        <h1>WELCoME TO OFFER PAGE!</h1>
    )
}
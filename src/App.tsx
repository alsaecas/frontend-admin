import React from 'react';
import { ChakraProvider } from '@chakra-ui/react';
import theme from '../src/theme/theme';
import "../src/assets/css/App.css";
import { BrowserRouter as Router } from 'react-router-dom'
import { AppRoutes } from './routes/Routes';



function App() {
  return (
    <ChakraProvider theme={theme}>
      <Router>
        <AppRoutes />
      </Router>
    </ChakraProvider>
  );
}

export default App;

import { BrowserRouter as Router, Routes, Route, Navigate, Link } from 'react-router-dom'
import { NewOfferPage } from '../pages/NewOfferPage';
import { OfferPage } from '../pages/OfferPage';
import { LoginPage } from '../pages/LoginPage';
import { HomePage } from '../pages/HomePage';


export const AppRoutes = () => {
  return (
    <Routes>
      <Route path='/' element={<HomePage />}></Route>
      <Route path='/login' element={<LoginPage />}></Route>
      <Route path='/offer' element={<OfferPage />}></Route>
      <Route path='/newoffer' element={<NewOfferPage />}></Route>
      {/* Redirect if no page */}
      <Route
        path='*'
        element={<Navigate to='/' replace />}>
      </Route>
    </Routes>
  )
}